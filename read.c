#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

/*
 *	Copyright (C) 2002-2005 Novell/SUSE
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation, version 2 of the
 *	License.
 */

/* Try to fork+exec @argv[1] using argv[1:] as argv[1]'s args */

int main(int argc, char *argv[])
{
  int fd;
  int my_errno;

  if (argc < 3){
      fprintf(stderr, "usage: %s expected_errno program [args] \n", argv[0]);
      return 1;
  }

  my_errno = atoi(argv[1]);

  fd = open(argv[2], O_RDONLY);
  
  if (fd >= 0)
    {
      if (my_errno == 0)
        puts("PASS");
      else
        printf("FAILED: wanted errno=%d but open was sucessful\n", my_errno);
    }
  else
    {
      if (my_errno == 0)
        {
          printf("ERRNO=%d but expected sucess\n", errno);
          fflush(NULL);
          perror("FAILED");
        }
      else if (my_errno == errno)
        {
          puts("PASS");
        }
      else
        {
          printf("ERRNO=%d but expected %d\n", errno, my_errno);
          fflush(NULL);
          perror("FAILED");
        }
    }

  return 0;
}
